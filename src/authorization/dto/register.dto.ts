import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsUrl,
  Length,
  NotContains,
} from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  displayName: string;

  // @IsNotEmpty()
  // @NotContains(' ')
  // username: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @NotContains(' ')
  password: string;

  @IsOptional()
  photo: string;

  @IsOptional()
  bio: string;

  @IsOptional()
  @IsUrl(undefined, { message: 'Company Url is not valid.' })
  website: string;

  @IsOptional()
  @Length(16, 16)
  npwp: string;

  @IsOptional()
  suratIzin: string;

  @IsOptional()
  aktaPerusahaan: string;
}
