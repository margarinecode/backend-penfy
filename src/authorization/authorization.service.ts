import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/register.dto';
import { Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Role } from 'src/users/entities/role.entity';
import { LoginDto } from './dto/login.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class authorizationService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Role)
    private roleRepository: Repository<Role>,
    private jwtService: JwtService,
  ) {}

  async create(createUserDto: CreateUserDto, roles) {
    const role = await this.roleRepository.findOne({
      where: {
        name: roles,
      },
    });

    if (!role) {
      throw new HttpException(
        {
          statusCode: HttpStatus.CONFLICT,
          error: 'role not found',
        },
        HttpStatus.CONFLICT,
      );
    }

    const basePassword = createUserDto.password;
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(basePassword, salt);
    const result = await this.usersRepository.insert({
      username: (Math.random() + 1).toString(36).substring(7),
      role: role,
      password: hash,
      salt: salt,
      email: createUserDto.email,
      photo: 'default.png',
      displayName: createUserDto.displayName,
      bio: createUserDto.bio,
      website: createUserDto.website,
      npwp: createUserDto.npwp,
      suratIzin: createUserDto.suratIzin,
      aktaPerusahaan: createUserDto.aktaPerusahaan,
    });

    return this.usersRepository.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
    });
  }

  async login(login: LoginDto) {
    try {
      const existUser = await this.usersRepository
        .createQueryBuilder('u')
        .innerJoinAndSelect('u.role', 'role')
        .where('u.username = :usn', { usn: login.username })
        .getOne();

      if (
        existUser &&
        (await bcrypt.compare(login.password, existUser.password))
      ) {
        const accesToken = this.jwtService.sign({
          existUser,
        });

        return accesToken;
      }

      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_FOUND,
          error: 'username atau password salah',
        },
        HttpStatus.NOT_FOUND,
      );
    } catch (e) {
      throw e;
    }
  }

  async findByUsername(usn) {
    const search = await this.usersRepository.findAndCount({
      where: { username: usn },
    });

    if (search[1] > 0) {
      return true;
    }

    return false;
  }
}
