import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import * as dotenv from 'dotenv';
import e from "express";
dotenv.config();

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {
    super({
      secretOrKey: `123`,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    });
  }

  async validate(payload: any) {
    const existUserData = await this.userRepository.findOne({
      where: {
        username: payload.existUser.username,
      },
      relations: ['role'],
    });

    if (!existUserData) {
      throw new HttpException(
        {
          statusCode: HttpStatus.UNAUTHORIZED,
          message: 'UNAUTHORIZED',
          DATA: 'TOKEN IS INVALID',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }

    return {
      id: existUserData.id,
      email: existUserData.email,
      role: existUserData.role.name,
    };
  }
}
