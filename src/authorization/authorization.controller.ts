import {
  Controller,
  Body,
  Param,
  HttpStatus,
  Post,
  HttpException,
} from '@nestjs/common';
import { CreateUserDto } from './dto/register.dto';
import { authorizationService } from './authorization.service';
import { LoginDto } from './dto/login.dto';

@Controller('auth')
export class authorizationController {
  constructor(private readonly authorizationService: authorizationService) {}

  @Post('/register/:role')
  async create(
    @Param('role') role: string,
    @Body() createUserDto: CreateUserDto,
  ) {
    // const hasil = await this.authorizationService.findByUsername(
    //   createUserDto.username,
    // );
    //
    // if (hasil == true) {
    //   throw new HttpException(
    //     {
    //       statusCode: HttpStatus.CONFLICT,
    //       error: 'username sudah terpakai',
    //     },
    //     HttpStatus.CONFLICT,
    //   );
    // } else if (hasil == false) {
    return {
      data: await this.authorizationService.create(createUserDto, role),
      statusCode: HttpStatus.CREATED,
      message: 'success',
      // };
    };
  }

  @Post('/login')
  async login(@Body() login: LoginDto) {
    return this.authorizationService.login(login);
  }
}
