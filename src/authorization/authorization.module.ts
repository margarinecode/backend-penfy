import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from 'src/users/entities/role.entity';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { JwtStrategy } from './jwt-strategies';
import * as dotenv from 'dotenv';
import { authorizationController } from './authorization.controller';
import { authorizationService } from './authorization.service';
dotenv.config();

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Role]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: `123`,
    }),
  ],

  controllers: [authorizationController],
  providers: [UsersService, JwtStrategy, ConfigService, authorizationService],
  exports: [JwtStrategy, PassportModule],
})
export class AuthorizationModule {}
