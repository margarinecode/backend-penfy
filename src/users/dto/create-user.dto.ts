import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsUrl,
  NotContains,
} from 'class-validator';

export class CreateUserDto {
  @IsOptional()
  displayName: string;

  @IsOptional()
  @NotContains(' ')
  username: string;

  @IsOptional()
  @IsEmail()
  email: string;

  @IsOptional()
  @NotContains(' ')
  password: string;

  @IsOptional()
  photo: string;

  @IsOptional()
  bio: string;

  @IsOptional()
  @IsUrl(undefined, { message: 'Company Url is not valid.' })
  website: string;
}
