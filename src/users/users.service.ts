import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { EntityNotFoundError, Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async findByUsername(username) {
    const search = await this.usersRepository.findAndCount({
      where: { username },
    });

    if (search[1] > 0) {
      return true;
    }

    return false;
  }

  async findOne(req) {
    try {
      return await this.usersRepository.findOneOrFail({
        where: {
          id: req,
        },
        relations: ['story'],
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    const data = await this.usersRepository.findOneOrFail({
      where: {
        id,
      },
    });
    const hash =
      updateUserDto.password !== null &&
      updateUserDto.password !== undefined &&
      (await bcrypt.hash(updateUserDto.password, data.salt));

    data.displayName = updateUserDto.displayName;
    data.username = updateUserDto.username;
    updateUserDto.password !== null && updateUserDto.password !== undefined
      ? (data.password = hash) :
      (data.password = data.password);
    data.email = updateUserDto.email;
    data.photo = updateUserDto.photo;
    data.bio = updateUserDto.bio;
    data.website = updateUserDto.website;
    data.updatedAt = new Date();

    await this.usersRepository.update(id, data);

    return this.usersRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }
}
