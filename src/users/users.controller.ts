import {
  Controller,
  Get,
  Body,
  Param,
  UseGuards,
  Req,
  HttpStatus,
  Put,
  ParseUUIDPipe,
  HttpException,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('/detail')
  @UseGuards(AuthGuard('jwt'))
  async findOne(@Req() req) {
    return {
      data: await this.usersService.findOne(req.user.id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('/detail-profile/:id')
  @UseGuards(AuthGuard('jwt'))
  async findProfile(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.usersService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put('/update/:id')
  @UseGuards(AuthGuard('jwt'))
  async update(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    const before = (await this.usersService.findOne(id)).username;
    let a;

    if (updateUserDto.username && updateUserDto.username !== before) {
      a = updateUserDto.username;
    } else {
      return {
        data: await this.usersService.update(id, updateUserDto),
        statusCode: HttpStatus.OK,
        message: 'success',
      };
    }

    const hasil = await this.usersService.findByUsername(a);

    if (hasil == true) {
      throw new HttpException(
        {
          statusCode: HttpStatus.CONFLICT,
          error: 'username already in use',
        },
        HttpStatus.CONFLICT,
      );
    } else if (hasil == false) {
      return {
        data: await this.usersService.update(id, updateUserDto),
        statusCode: HttpStatus.OK,
        message: 'success',
      };
    }
  }
}
