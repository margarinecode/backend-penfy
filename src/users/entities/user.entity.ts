import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Role } from './role.entity';
import { Story } from '../../story/entities/story.entity';
import { Collection } from '../../collection/entities/collection.entity';
import { Additional_data_story } from '../../story/entities/addtional_data_story.entity';
import { Additional_data_story_eps } from '../../story/entities/additional_data_story_eps.entity';
import { Comment } from '../../story/entities/comment.entity';
import { Request } from '../../request/entities/request.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  email: string;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column()
  salt: string;

  @Column()
  displayName: string;

  @Column()
  bio: string;

  @Column()
  photo: string;

  @Column({ nullable: true })
  website: string;

  @Column({ nullable: true })
  npwp: string;

  @Column({ nullable: true })
  suratIzin: string;

  @Column({ nullable: true })
  aktaPerusahaan: string;

  @CreateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp with time zone',
    nullable: true,
  })
  deletedAt: Date;

  @ManyToOne(() => {
    return Role;
  })
  role: Role;

  @OneToMany(
    () => {
      return Story;
    },
    (story) => {
      return story.user;
    },
  )
  story: Story;

  @OneToMany(
    () => {
      return Collection;
    },
    (collection) => {
      return collection.user;
    },
  )
  collection: Collection;

  @OneToMany(
    () => {
      return Additional_data_story;
    },
    (additional_data_story) => {
      return additional_data_story.user;
    },
  )
  additional_data_story: Additional_data_story;

  @OneToMany(
    () => {
      return Additional_data_story_eps;
    },
    (additional_data_story_eps) => {
      return additional_data_story_eps.user;
    },
  )
  additional_data_story_eps: Additional_data_story_eps;

  @OneToMany(
    () => {
      return Comment;
    },
    (comment) => {
      return comment.user;
    },
  )
  comment: Comment;

  @OneToMany(
    () => {
      return Request;
    },
    (request) => {
      return request.story;
    },
  )
  request: Request;
}
