import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { Story } from '../../story/entities/story.entity';

@Entity()
export class Request {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  text: string;

  @Column()
  status: string;

  @Column()
  requestedBy: string;

  @ManyToOne(() => {
    return User;
  })
  user: User;

  @ManyToOne(() => {
    return Story;
  })
  story: Story;
}
