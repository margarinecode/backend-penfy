import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  ParseUUIDPipe,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { RequestService } from './request.service';
import { CreateRequestDto } from './dto/create-request.dto';
import { UpdateRequestDto } from './dto/update-request.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('request')
export class RequestController {
  constructor(private readonly requestService: RequestService) {}

  @Post('/create/:id_story')
  @UseGuards(AuthGuard('jwt'))
  async create(
    @Body() createRequestDto: CreateRequestDto,
    @Req() req,
    @Param('id_story', ParseUUIDPipe) id_story: string,
  ) {
    const result = await this.requestService.create(
      createRequestDto,
      req.user,
      id_story,
    );

    return {
      data: result,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get()
  findAll() {
    return this.requestService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.requestService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRequestDto: UpdateRequestDto) {
    return this.requestService.update(+id, updateRequestDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.requestService.remove(+id);
  }
}
