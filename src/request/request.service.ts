import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateRequestDto } from './dto/create-request.dto';
import { UpdateRequestDto } from './dto/update-request.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from './entities/request.entity';
import { User } from '../users/entities/user.entity';
import { Story } from '../story/entities/story.entity';

@Injectable()
export class RequestService {
  constructor(
    @InjectRepository(Request)
    private requestRepository: Repository<Request>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Story)
    private storyRepository: Repository<Story>,
  ) {}

  async create(createRequestDto: CreateRequestDto, req, id_story) {
    const usr = await this.userRepository.findOne({
      where: {
        id: req.id,
      },
      relations: ['role'],
    });
    const story = await this.storyRepository.findOne({
      where: {
        id: id_story,
      },
    });

    const old = await this.requestRepository.findOne({
      where: {
        user: {
          id: usr.id,
        },
        story: {
          id: story.id,
        },
      },
    });

    if (old && old?.status !== 'Declined') {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_FOUND,
          error: 'Request already exist',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    if (!story) {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_FOUND,
          error: 'Story not found',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const data = new Request();

    data.user = usr;
    data.requestedBy = usr.role.name;
    data.text = createRequestDto.text;
    data.story = story;
    data.status = 'Requested';

    const result = await this.requestRepository.insert(data);

    return this.requestRepository.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
    });
  }

  findAll() {
    return `This action returns all request`;
  }

  findOne(id: number) {
    return `This action returns a #${id} request`;
  }

  update(id: number, updateRequestDto: UpdateRequestDto) {
    return `This action updates a #${id} request`;
  }

  remove(id: number) {
    return `This action removes a #${id} request`;
  }
}
