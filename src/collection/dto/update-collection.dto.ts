import { PartialType } from '@nestjs/swagger';
import { CreateCollectionDto } from './create-collection.dto';
import { IsOptional } from 'class-validator';

export class UpdateCollectionDto extends PartialType(CreateCollectionDto) {
  @IsOptional()
  last_eps: number;
}
