import { IsOptional } from 'class-validator';

export class CreateCollectionDto {
  @IsOptional()
  id_story: string;
}
