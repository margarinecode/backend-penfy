import { Module } from '@nestjs/common';
import { CollectionService } from './collection.service';
import { CollectionController } from './collection.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Collection } from './entities/collection.entity';
import { Story } from '../story/entities/story.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Collection, Story])],
  controllers: [CollectionController],
  providers: [CollectionService],
})
export class CollectionModule {}
