import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateCollectionDto } from './dto/create-collection.dto';
import { UpdateCollectionDto } from './dto/update-collection.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Collection } from './entities/collection.entity';
import { EntityNotFoundError, Repository } from 'typeorm';
import { Story } from '../story/entities/story.entity';

@Injectable()
export class CollectionService {
  constructor(
    @InjectRepository(Collection)
    private collectionRepository: Repository<Collection>,
    @InjectRepository(Story)
    private storyRepository: Repository<Story>,
  ) {}

  async create(id_story, req) {
    let story;

    try {
      story = await this.storyRepository.findOneOrFail({
        where: {
          id: id_story,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    //search for duplicate
    const dup = await this.collectionRepository.findOne({
      where: {
        user: req,
        story: { id: story.id },
      },
    });

    if (dup) {
      throw new HttpException(
        {
          statusCode: HttpStatus.BAD_REQUEST,
          error: 'Data duplicate',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const result = await this.collectionRepository.insert({
      status: 'UNREAD',
      last_eps: 0,
      user: req,
      story: story,
    });

    return this.collectionRepository.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
    });
  }

  async findAll(status, req) {
    const queryBuilder = this.collectionRepository
      .createQueryBuilder('c')
      .innerJoinAndSelect('c.user', 'user')
      .innerJoinAndSelect('c.story', 'story')
      .where('user.id', req.id)
      .orderBy('c.updatedAt', 'DESC');

    if (status) {
      queryBuilder.where('c.status = :status', { status });
    }

    return await queryBuilder.getMany();
  }

  findOne(id: number) {
    return `This action returns a #${id} collection`;
  }

  async update(id, updateCollectionDto: UpdateCollectionDto) {
    const data = await this.collectionRepository.findOneOrFail({
      where: {
        id,
      },
    });
    // const storyData = await this.storyRepository.findOneOrFail({
    //   where: {
    //     id: data.id,
    //   },
    // });
    // if(data.last_eps > 1)
    data.last_eps = updateCollectionDto.last_eps;
  }

  remove(id: number) {
    return `This action removes a #${id} collection`;
  }
}
