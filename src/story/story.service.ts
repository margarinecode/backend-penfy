import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateStoryDto } from './dto/create-story.dto';
import { UpdateStoryDto } from './dto/update-story.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';
import { EntityNotFoundError, Repository } from 'typeorm';
import { Story } from './entities/story.entity';
import { Additional_data_story } from './entities/addtional_data_story.entity';

@Injectable()
export class StoryService {
  constructor(
    @InjectRepository(Story)
    private storyRepository: Repository<Story>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Additional_data_story)
    private additionalDataStoryRepository: Repository<Additional_data_story>,
  ) {}

  async create(createStoryDto: CreateStoryDto, req) {
    const result = await this.storyRepository.insert({
      cover: createStoryDto.cover,
      description: createStoryDto.description,
      genre: createStoryDto.genre,
      status: 'NOT_PUBLISHED',
      likes: 0,
      publisher: undefined,
      tags: createStoryDto.tags,
      title: createStoryDto.title,
      user: req.id,
      publicity: createStoryDto.publicity,
    });

    return this.storyRepository.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
    });
  }

  async findAll(search) {
    const queryBuilderStory = this.storyRepository
      .createQueryBuilder('s')
      .innerJoinAndSelect('s.user', 'user')
      .orderBy('s.updatedAt', 'DESC');

    const queryBuilderPublisher = this.userRepository
      .createQueryBuilder('u')
      .innerJoinAndSelect('u.role', 'r')
      .where('r.name = :role', { role: 'publisher' })
      .orderBy('u.updatedAt', 'DESC');

    if (search) {
      queryBuilderStory
        .where('s.genre ilike :genre', { genre: `%${search}%` })
        .orWhere('s.title ilike :title', { title: `%${search}%` })
        .orWhere(':search = ANY (s.tags)', { search });

      queryBuilderPublisher
        .orWhere('u.username ilike :username', { username: `%${search}%` })
        .orWhere('u.displayName ilike :displayName', {
          displayName: `%${search}%`,
        });
    }

    return {
      story: await queryBuilderStory.getMany(),
      publisher: await queryBuilderPublisher.getMany(),
    };
  }

  async myStory(req) {
    const result = await this.storyRepository.findAndCount({
      where:
        req.role === 'reader'
          ? {
              user: {
                id: req.id,
              },
            }
          : {
              publisher: {
                id: req.id,
              },
            },
    });

    return result;
  }

  async findOne(id, req) {
    try {
      const result = await this.storyRepository.findOneOrFail({
        where: {
          id,
        },
        relations: ['user', 'story_eps', 'additional_data_story'],
      });

      await this.additionalData(id, req, 'seen');

      return result;
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async additionalData(id, req, type) {
    const story = await this.storyRepository.findOne({
      where: {
        id,
      },
    });

    const data = await this.additionalDataStoryRepository.findOne({
      where: {
        user: {
          id: req.id,
        },
        story: {
          id,
        },
        type,
      },
    });

    if (!data) {
      await this.additionalDataStoryRepository.insert({
        user: req,
        story: story,
        type: type,
      });
    }
  }

  async update(id: string, updateStoryDto: UpdateStoryDto) {
    const data = await this.storyRepository.findOneOrFail({
      where: {
        id,
      },
    });

    await this.storyRepository.update(id, {
      ...data,
      ...updateStoryDto,
    });

    return this.storyRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  async remove(id) {
    try {
      await this.storyRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.storyRepository.softDelete(id);
  }
}
