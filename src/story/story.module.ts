import { Module } from '@nestjs/common';
import { StoryService } from './story.service';
import { StoryController } from './story.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Story } from './entities/story.entity';
import { Story_eps } from './entities/story_eps.entity';
import { StoryEpsService } from './story_eps.service';
import { StoryEpsController } from './story_eps.controller';
import { Additional_data_story } from './entities/addtional_data_story.entity';
import { Additional_data_story_eps } from './entities/additional_data_story_eps.entity';
import { Comment } from './entities/comment.entity';
import { Collection } from '../collection/entities/collection.entity';
import { User } from '../users/entities/user.entity';
import { CommentController } from './comment.controller';
import { CommentService } from './comment.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Story,
      Story_eps,
      Additional_data_story,
      Additional_data_story_eps,
      User,
      Comment,
      Collection,
    ]),
  ],
  controllers: [StoryController, StoryEpsController, CommentController],
  providers: [StoryService, StoryEpsService, CommentService],
})
export class StoryModule {}
