import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { Story_eps } from './story_eps.entity';

@Entity()
export class Additional_data_story_eps {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  type: string;

  @ManyToOne(() => {
    return User;
  })
  user: User;

  @ManyToOne(() => {
    return Story_eps;
  })
  story_eps: Story_eps;
}
