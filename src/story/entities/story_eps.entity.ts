import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { Comment } from './comment.entity';
import { Story } from './story.entity';
import { Additional_data_story_eps } from './additional_data_story_eps.entity';
import { Additional_data_story } from './addtional_data_story.entity';

@Entity()
export class Story_eps {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  title: string;

  @Column()
  content: string;

  @Column()
  order: number;

  @CreateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp with time zone',
    nullable: true,
  })
  deletedAt: Date;

  @ManyToOne(() => {
    return Story;
  })
  story: Story;

  @OneToMany(
    () => {
      return Additional_data_story_eps;
    },
    (additional_data_story_eps) => {
      return additional_data_story_eps.story_eps;
    },
  )
  additional_data_story_eps: Additional_data_story;

  @OneToMany(
    () => {
      return Comment;
    },
    (comment) => {
      return comment.story_eps;
    },
  )
  comment: Comment;
}
