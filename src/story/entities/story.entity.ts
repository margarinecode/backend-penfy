import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { Request } from '../../request/entities/request.entity';
import { Collection } from '../../collection/entities/collection.entity';
import { Story_eps } from './story_eps.entity';
import { Additional_data_story } from './addtional_data_story.entity';

@Entity()
export class Story {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column('varchar', { array: true })
  tags: string[];

  @Column()
  cover: string;

  @Column()
  likes: number;

  @Column()
  status: string;

  @Column()
  genre: string;

  @Column()
  publicity: string;

  @Column({ nullable: true })
  isbn: string;

  @CreateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp with time zone',
    nullable: true,
  })
  deletedAt: Date;

  @ManyToOne(() => {
    return User;
  })
  user: User;

  @ManyToOne(() => {
    return User;
  })
  publisher: User;

  @OneToMany(
    () => {
      return Collection;
    },
    (collection) => {
      return collection.story;
    },
  )
  collection: Collection;

  @OneToMany(
    () => {
      return Story_eps;
    },
    (story_eps) => {
      return story_eps.story;
    },
  )
  story_eps: Story_eps;

  @OneToMany(
    () => {
      return Additional_data_story;
    },
    (additional_data_story) => {
      return additional_data_story.story;
    },
  )
  additional_data_story: Additional_data_story;

  @OneToMany(
    () => {
      return Request;
    },
    (request) => {
      return request.story;
    },
  )
  request: Request;
}
