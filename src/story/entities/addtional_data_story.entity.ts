import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { Story } from './story.entity';

@Entity()
export class Additional_data_story {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  type: string;

  @ManyToOne(() => {
    return User;
  })
  user: User;

  @ManyToOne(() => {
    return Story;
  })
  story: Story;
}
