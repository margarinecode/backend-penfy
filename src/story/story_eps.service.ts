import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Story_eps } from './entities/story_eps.entity';
import { EntityNotFoundError, Repository } from 'typeorm';
import { CreateStoryEpsDto } from './dto/create-story_eps.dto';
import { Story } from './entities/story.entity';
import { Collection } from '../collection/entities/collection.entity';
import { Additional_data_story_eps } from './entities/additional_data_story_eps.entity';

@Injectable()
export class StoryEpsService {
  constructor(
    @InjectRepository(Story_eps)
    private storyEpsRepository: Repository<Story_eps>,
    @InjectRepository(Story)
    private storyRepository: Repository<Story>,
    @InjectRepository(Collection)
    private collectionRepository: Repository<Collection>,
    @InjectRepository(Additional_data_story_eps)
    private additionalDataStoryEpsRepository: Repository<Additional_data_story_eps>,
  ) {}

  async create(id_story, createStoryepsDto: CreateStoryEpsDto) {
    let story;

    try {
      story = await this.storyRepository.findOneOrFail({
        where: {
          id: id_story,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    const order = await this.storyEpsRepository
      .createQueryBuilder('e')
      .innerJoinAndSelect('e.story', 's')
      .where('s.id = :s', { s: id_story })
      .select('MAX(e.order)', 'max')
      .getRawOne();

    const result = await this.storyEpsRepository.insert({
      story,
      title: createStoryepsDto.title,
      content: createStoryepsDto.content,
      order: order.max === null ? 1 : order.max + 1,
    });

    return this.storyEpsRepository.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
    });
  }

  async additionalData(id, req, type) {
    const eps = await this.storyEpsRepository.findOne({
      where: {
        id,
      },
    });

    const data = await this.additionalDataStoryEpsRepository.findOne({
      where: {
        user: {
          id: req.id,
        },
        story_eps: {
          id,
        },
        type,
      },
    });

    if (!data) {
      await this.additionalDataStoryEpsRepository.insert({
        user: req,
        story_eps: eps,
        type: type,
      });
    }
  }

  async findOne(id, req) {
    const eps = await this.storyEpsRepository.findOne({
      where: {
        id,
      },
      relations: ['additional_data_story_eps', 'comment', 'story'],
    });

    await this.additionalData(id, req, 'seen');

    return eps;
  }

  async findInCollection(order, idStory, req) {
    const eps = await this.storyEpsRepository.findOne({
      where: {
        order,
        story: {
          id: idStory,
        },
      },
      relations: ['additional_data_story_eps', 'comment', 'story'],
    });

    const inCollection = await this.collectionRepository.findOne({
      where: {
        user: {
          id: req.id,
        },
        story: {
          id: idStory,
        },
      },
    });

    const totalEps = await this.storyEpsRepository.findAndCount({
      where: {
        story: {
          id: idStory,
        },
      },
    });

    await this.collectionRepository.update(inCollection.id, {
      status: totalEps[1] == eps.order ? 'FINISHED' : 'ONGOING',
      last_eps: eps.order,
    });

    await this.additionalData(eps.id, req, 'seen');

    return eps;
  }
}
