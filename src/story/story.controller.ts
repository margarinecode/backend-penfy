import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
  HttpStatus,
  Query,
  Put,
  ParseUUIDPipe,
} from '@nestjs/common';
import { StoryService } from './story.service';
import { CreateStoryDto } from './dto/create-story.dto';
import { UpdateStoryDto } from './dto/update-story.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('story')
export class StoryController {
  constructor(private readonly storyService: StoryService) {}

  @Post('/create')
  @UseGuards(AuthGuard('jwt'))
  async create(@Body() createStoryDto: CreateStoryDto, @Req() req) {
    return {
      data: await this.storyService.create(createStoryDto, req.user),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async findAll(@Query('search') search: string) {
    return {
      data: await this.storyService.findAll(search),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
  @Get('/myStory')
  @UseGuards(AuthGuard('jwt'))
  async myStory(@Req() req) {
    return {
      data: await this.storyService.myStory(req.user),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async findOne(@Param('id') id: string, @Req() req) {
    return {
      data: await this.storyService.findOne(id, req.user),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
  @Post('like/:id')
  @UseGuards(AuthGuard('jwt'))
  async like(@Param('id', ParseUUIDPipe) id: string, @Req() req) {
    return {
      data: await this.storyService.additionalData(id, req.user, 'like'),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put('/update/:id')
  @UseGuards(AuthGuard('jwt'))
  async update(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateStoryDto: UpdateStoryDto,
  ) {
    return {
      data: await this.storyService.update(id, updateStoryDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete('/delete/:id')
  @UseGuards(AuthGuard('jwt'))
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    await this.storyService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
