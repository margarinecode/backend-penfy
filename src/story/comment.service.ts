import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateStoryDto } from './dto/create-story.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Comment } from './entities/comment.entity';
import { CreateCommentDto } from './dto/create-comment.dto';
import { User } from '../users/entities/user.entity';
import { Story_eps } from './entities/story_eps.entity';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private commentRepository: Repository<Comment>,

    @InjectRepository(User)
    private userRepository: Repository<User>,

    @InjectRepository(Story_eps)
    private storyEpsRepository: Repository<Story_eps>,
  ) {}

  async create(createCommentDto: CreateCommentDto, req, id_story_eps) {
    const usr = await this.userRepository.findOne({
      where: {
        id: req.id,
      },
    });

    const storyEps = await this.storyEpsRepository.findOne({
      where: {
        id: id_story_eps,
      },
    });

    if (!usr || !storyEps) {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_FOUND,
          error: 'Data not found',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const result = await this.commentRepository.insert({
      content: createCommentDto.content,
      user: usr,
      story_eps: storyEps,
    });

    return this.commentRepository.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
    });
  }
}
