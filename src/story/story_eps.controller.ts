import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  ParseUUIDPipe,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { StoryEpsService } from './story_eps.service';
import { AuthGuard } from '@nestjs/passport';
import { CreateStoryEpsDto } from './dto/create-story_eps.dto';

@Controller('story-eps')
export class StoryEpsController {
  constructor(private readonly storyEpsService: StoryEpsService) {}

  @Post('create/:id_story')
  @UseGuards(AuthGuard('jwt'))
  async create(
    @Param('id_story', ParseUUIDPipe) id_story: string,
    @Body() createStoryEpsDto: CreateStoryEpsDto,
  ) {
    const data = await this.storyEpsService.create(id_story, createStoryEpsDto);
    return {
      data,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async findOne(@Param('id', ParseUUIDPipe) id: string, @Req() req) {
    return {
      data: await this.storyEpsService.findOne(id, req.user),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get(':idStory/:order')
  @UseGuards(AuthGuard('jwt'))
  async findInCollection(
    @Param('idStory', ParseUUIDPipe) idStory: string,
    @Param('order') order: number,
    @Req() req,
  ) {
    return {
      data: await this.storyEpsService.findInCollection(
        order,
        idStory,
        req.user,
      ),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Post('like/:id')
  @UseGuards(AuthGuard('jwt'))
  async like(@Param('id', ParseUUIDPipe) id: string, @Req() req) {
    return {
      data: await this.storyEpsService.additionalData(id, req.user, 'like'),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
