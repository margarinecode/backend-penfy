import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
  HttpStatus,
  ParseUUIDPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CommentService } from './comment.service';
import { CreateCommentDto } from './dto/create-comment.dto';

@Controller('comment')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @Post('/create/:id_eps')
  @UseGuards(AuthGuard('jwt'))
  async create(
    @Body() createCommentDto: CreateCommentDto,
    @Req() req,
    @Param('id_eps', ParseUUIDPipe) id_eps: string,
  ) {
    return {
      data: await this.commentService.create(
        createCommentDto,
        req.user,
        id_eps,
      ),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
