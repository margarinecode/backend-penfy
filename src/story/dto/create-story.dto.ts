import { IsArray, IsOptional } from 'class-validator';

export class CreateStoryDto {
  @IsOptional()
  title: string;

  @IsOptional()
  description: string;

  @IsOptional()
  @IsArray()
  tags: string[];

  @IsOptional()
  genre: string;

  @IsOptional()
  cover: string;
  @IsOptional()
  publicity: string;
}
