import { IsOptional } from 'class-validator';

export class CreateStoryEpsDto {
  @IsOptional()
  title: string;

  @IsOptional()
  content: string;
}
